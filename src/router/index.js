import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import VFor from '../views/VFor.vue'
import Vuex from '../views/Vuex.vue'
import Grid from '../views/Grid.vue'
import Modal from '../views/Modal.vue'
import Table from '../views/Table.vue'
import Form from '../views/Form.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/vIf',
    name: 'VIf',
    component: () => import('../views/VIf.vue')
  },
  {
    path: '/vFor',
    name: 'VFor',
    component: VFor
  },
  {
    path: '/vuex',
    name: 'Vuex',
    component: Vuex
  },
  {
    path: '/grid',
    name: 'Grid',
    component: Grid
  },
  {
    path: '/modal',
    name: 'Modal',
    component: Modal
  },
  {
    path: '/table',
    name: 'Table',
    component: Table
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  }
]

const router = new VueRouter({
  routes
})

export default router
