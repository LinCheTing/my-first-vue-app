export default {
  state: {
    account: sessionStorage.getItem('account')
  },
  mutations: {
    SET_ACCOUNT: (state, account) => {
      state.account = account
    }
  },
  actions: {
    submitLogin ({ commit }, data) {
      const account = data.account
      sessionStorage.setItem('account', account)
      commit('SET_ACCOUNT', account)
    },
    Logout ({ commit }) {
      commit('SET_ACCOUNT', '')
      sessionStorage.clear()
    }
  }
}
